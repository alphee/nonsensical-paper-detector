# Nonsensical Paper Detector

This repository contains the scripts used to detect nonsensical papers (see our [_JASIST_ paper](https://doi.org/10.1002/asi.24495)).

Ensure that your have Python 3.9+ (python3 --version)

## Install dependencies
```
pip3 install dimcli

dimcli --init

pip3 install objectpath
```

## Run the detector
```
python3 nonsensicalPaperDetector.py scigen_candidate_phrases.txt
```

## Further reading

Cabanac, G., Labbé, C. (2021).
Prevalence of nonsensical algorithmically-generated papers in the scientific literature, 
_Journal of the Association for Information Science and Technology_. https://doi.org/10.1002/asi.24495 [supporting information](https://doi.org/10.5281/zenodo.4729758)


# Problematic Paper Screener

The nonsensical paper detector is run regularly.
It combs the literature for algorithmically generated based on the [dada](https://dev.null.org/dadaengine/), [mathgen](https://thatsmathematics.com/blog/mathgen/), [sbir](https://www.nadovich.com/chris/randprop/), and [SCIgen](https://pdos.csail.mit.edu/scigen/) grammars.

See the latest results: https://www.irit.fr/~Guillaume.Cabanac/problematic-paper-screener
