#!/usr/bin/env python3
# Nonsensical Paper Detector
# Run fingerprint-queries (e.g., SCIgen excerpts) against Dimensions.ai and download results (i.e., bibliographic records) 
# @author <a href="mailto:guillaume.cabanac@univ-tlse3.fr">Guillaume Cabanac</a>
# @author <a href="mailto:cyril.labbe@univ-grenoble-alpes.fr">Cyril Labbé</a>
# @since   29-APR-2020
# @version 20-MAY-2020
import dimcli
from objectpath import Tree
from pathlib  	import Path
from requests   import exceptions, post
from sys      	import argv, exit
from time       import sleep, strftime

def getPhrases(ficnamePhrases):
	with open(ficnamePhrases) as ficPhrases:
		"""Filter lines of `ficQuery` and retain only those starting and ending with `"`, escaping the quotes for Dimensions"""
		lines = list(map(lambda l: l.strip(), list(ficPhrases)))
		lines = list(filter(lambda l: l.startswith('"') and l.endswith('"'), lines))
		lines = list(map(lambda l: l.replace('"', '\\"'), lines))
		return lines

def craftQuery(phrase):  # https://app.dimensions.ai/dsl
	"""Prepare a Dimensions query out of a given phrase"""
	return f"""search publications in full_data for "{phrase}"
	           return publications [id+year+type+doi+title+journal+proceedings_title+publisher+book_title+open_access_categories+times_cited+altmetric+linkout]
			   limit 1000"""

def executeQuery(dsl, query):
	"""Search for the query via Dimensions and return results in JSON"""
	try:
		jsonData = dsl.query(query)
	except exceptions.HTTPError as he:
		# Error handling when HTTP errors occur(e.g., 500 Bad Gateway)
		print(f'ERROR: Dimensions API HTTP error for query:\n{query}\nDetails: {he}\n\nRetrying after sleep.')
		sleep(20)  # The Dimensions Analytics API is limited to 30 requests per IP address per minute. (https://docs.dimensions.ai/dsl/api.html)
		return executeQuery(dsl, query)
	return jsonData

def formatList(lst):
	"""Returns list elements separated by a bullet point """
	return ', '.join(list(lst)) if lst != None else ''

def json2tsv(phraseNbr, jsonData, tsvOutFile):
	"""Format the JSON result in TSV"""
	def vv(dict, key):
		"""Remove special characters and return empty string instead of None"""
		value = dict.get(key)
		return str(value).replace('\t', ' ').replace('\n', ' *** ') if value != None else ''
	for pub in jsonData["publications"]:
		treePub = Tree(pub)
		venue = treePub.execute('$.journal.title') or pub.get('proceedings_title') or pub.get('book_title')
		oaStatus = formatList(treePub.execute('$.open_access_categories.id'))
		tsvOutFile.write(f"{phraseNbr}\t{vv(pub, 'id')}\t{vv(pub, 'year')}\t{vv(pub, 'type')}\t{vv(pub, 'times_cited')}\t{vv(pub, 'altmetric')}\t{vv(pub, 'doi')}\t"+
		                 f"{oaStatus}\t{venue}\t{vv(pub, 'title')}\t{vv(pub, 'publisher')}\t{vv(pub, 'linkout')}\n")

def sniffFakePapers(dsl, ficnamePhrases):
	lstPhrases = getPhrases(ficnamePhrases)
	ficnamePhrasesBasename = Path(ficnamePhrases).stem
	ficnameOutput = f"{strftime('%Y%m%d-%H%M%S')}_{ficnamePhrasesBasename}"
	with open(f"{ficnameOutput}_phrases.tsv", "w", encoding="utf-8") as outPhrases, open(f"{ficnameOutput}_records.tsv", "w", encoding="utf-8") as outRecords:
		outPhrases.write("phraseNbr	phrase\n")
		outRecords.write("phraseNbr	pubid	year	doctype	timesCited	altmetricScore	doi	oaStatus	journalOrProceedings	title	publisher	linkout\n")
		for i, phrase in enumerate(lstPhrases):
			idPhrase = str(i+1)
			outPhrases.write(f"{idPhrase}\t" + phrase.replace('\\', '') + "\n")
			jsonData = executeQuery(dsl, craftQuery(phrase))
			json2tsv(idPhrase, jsonData, outRecords)
			print(f"Phrase {idPhrase}: results written with {len(jsonData['publications'])} publications")
			
if __name__ == '__main__':
	"""Check presence of arguments and run detector on phrases given in each argument file"""
	if len(argv) == 1:
		print('Missing argument: Query file(s)')
		exit(1)
	dimcli.login() # we hypothesise ‘dimcli --init’ was run before
	dsl = dimcli.Dsl()
	for ficQuery in argv[1:]:
		sniffFakePapers(dsl, ficQuery)